package main

import (
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"strconv"

	"github.com/gin-gonic/gin"
	"github.com/joho/godotenv"
	"gitlab.com/controllercubiomes/backend/db"
)

func loadEnvVar() {
	err := godotenv.Load()
	if err != nil {
		log.Println("Error loading .env file")
	}
}

func main() {
	loadEnvVar()
	dbCred := db.DBCred{ //TODO: REMOVE THIS AND USE ENV VARIABLES
		DB_HOST:     "postgres",
		DB_USER:     "rockerpgadmin",
		DB_PASSWORD: "dLBgwTjDSq3IdStzVXFeU",
		DB_NAME:     "thisseedrocks",
	}
	conn, err := dbCred.ConnectToDB()
	if err != nil {
		log.Fatal(err)
	}
	dbc := db.DB{Conn: conn}
	fmt.Println("vim-go")
	r := gin.Default()
	{
		r.GET("api/registersystem", func(c *gin.Context) {
			key, err := dbc.RegisterSystemKey()
			if err != nil {
				log.Println(err)
				//TODO: handle
			}
			c.JSON(200, gin.H{"key": key})
		})
		r.GET("api/verified", func(c *gin.Context) {
			var page, limit string = "0", "50"
			pages, ok := c.Request.URL.Query()["page"]
			if ok || len(pages) > 0 {
				page = string(pages[0])
			}
			limits, ok := c.Request.URL.Query()["limit"]
			if ok || len(limits) > 0 {
				limit = string(limits[0])
			}
			seeds, err := dbc.GetVerifiedSeeds(page, limit)
			if err != nil {
				log.Println(err)
				//TODO: handle
			}
			if err := json.NewEncoder(c.Writer).Encode(seeds); err != nil {
				log.Println(err)
			}
		})
		r.GET("api/notverified", func(c *gin.Context) {
			var page, limit string = "0", "50"
			pages, ok := c.Request.URL.Query()["page"]
			if ok || len(pages) > 0 {
				page = string(pages[0])
			}
			limits, ok := c.Request.URL.Query()["limit"]
			if ok || len(limits) > 0 {
				limit = string(limits[0])
			}
			seeds, err := dbc.GetNotVerifiedSeeds(page, limit)
			if err != nil {
				log.Println(err)
				//TODO: handle
			}
			if err := json.NewEncoder(c.Writer).Encode(seeds); err != nil {
				log.Println(err)
			}
		})
		r.GET("api/seed/:seed", func(c *gin.Context) {
			seedID, err := strconv.ParseInt(c.Param("seed"), 10, 64)
			if err != nil {
				log.Println(err)
				c.JSON(http.StatusBadRequest, gin.H{"Error": err})
				return
			}
			seed, err := dbc.GetSeed(seedID)
			if err != nil {
				log.Println(err)
				c.JSON(500, gin.H{"Error": err})
				return
			}
			if err := json.NewEncoder(c.Writer).Encode(seed); err != nil {
				log.Println(err)
			}
		})
	}
	if err := r.Run(":3000"); err != nil {
		log.Println(err)
	}
}
